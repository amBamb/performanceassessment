package com.amber.perfomanceassessment;

import android.net.Uri;

public interface ActivityCallBack {
    void onPostSelected(int pos);
}
