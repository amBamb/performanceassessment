package com.amber.perfomanceassessment;

import android.net.Uri;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

public class PerformanceAssessment extends SingleFragmentActivity implements ActivityCallBack {

    public ArrayList<ToDoPost> toDoPosts = new ArrayList<>();

    @Override
    protected Fragment createFragment() {
        return new ToDoListFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    public void onPostSelected(int pos) {

    }
}
