package com.amber.perfomanceassessment;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoHolder>{

    private ActivityCallBack activityCallBack;
    private ArrayList<ToDoPost> todoItems;

    public ToDoAdapter(ArrayList<ToDoPost> todoItems, ActivityCallBack activityCallBack ){
        this.activityCallBack = activityCallBack;
        this.todoItems = todoItems;
            }


        @Override
        public ToDoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ToDoHolder(view);
    }

    @Override
        public void onBindViewHolder(ToDoHolder holder, int position) {
        holder.titleText.setText(todoItems.get(position).title);

    }

    @Override
        public int getItemCount() {
        return todoItems.size();
    }
}