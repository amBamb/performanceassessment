package com.amber.perfomanceassessment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ToDoListFragment extends Fragment{

    private RecyclerView recyclerView;
    private ActivityCallBack activityCallBack;
    private PerformanceAssessment activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activityCallBack = (ActivityCallBack) activity;
        this.activity = (PerformanceAssessment) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallBack = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_performance, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoPost i1 = new ToDoPost("August: Amber's Birthday", "", "dateAdded", "dueDate");
        ToDoPost i2 = new ToDoPost("November: Thanksgiving", "", "random", "randomFan");
        ToDoPost i3 = new ToDoPost("December: Christmas", "", "Drake", "JCole");
        ToDoPost i4 = new ToDoPost("January: New Year's", "", "ThatAin't", "Wiz");

        activity.toDoPosts.add(i1);
        activity.toDoPosts.add(i2);
        activity.toDoPosts.add(i3);
        activity.toDoPosts.add(i4);

        ToDoAdapter adapter = new ToDoAdapter(activity.toDoPosts, activityCallBack);
        recyclerView.setAdapter(adapter);

        return view;
    }
}
